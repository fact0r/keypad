#include "startup.h"
#include "gpio.h"
#include "ascii.h"

void app_init(void) {
    #ifdef USBDM
        * ( (unsigned long *) 0x4002380 ) = 0x18;
    #endif
    GPIO_D.moder = 0x55005555;
}

void activateRow(char row) {
    GPIO_D.odrHigh = 1<<(row+4);
}

unsigned char readColumn(void) {
    return GPIO_D.idrHigh;
}

unsigned char charFromKey(unsigned char row, unsigned char col) {
    //select least significant bit
    for(char i = 0; i<4;i++){
        if (((col>>i)&1) != 0) {
            col = i;
            break;
        }
    }
    //Array with values of keys
    char keyVal[4][4] = {
        {1, 2, 3, 10},
        {4, 5, 6, 11},
        {7, 8, 9, 12},
        {14,0,15, 13}
    };
    return keyVal[row][col];
}

unsigned char keyb(void) {
    for(char row = 0; row<4; row++) {
        activateRow(row);
        unsigned char col = readColumn();
        if(col != 0) {
            return charFromKey(row, col);
        }
    }
    return 0xFF;
}

void out7seg(unsigned char i) {
    GPIO_D.odrLow = CHARACTERS[i];
}

int main(void) {
    app_init();

    while(1){
        out7seg(keyb());
    }
}
